Name:           lensfun
Version:        0.3.4
Release:        3
Summary:        Library to correct defects introduced by photographic lenses
License:        LGPL-3.0-only and CC-BY-SA-3.0
URL:            https://lensfun.github.io/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz

Patch0001:      Pull-isnan-into-std-namespace-include-cmath-not-math.patch
Patch0002:      lensfun-0.3.4-port-to-newer-cmake.patch

BuildRequires:  cmake >= 3.12 doxygen gcc-c++ pkgconfig(glib-2.0) pkgconfig(libpng)
BuildRequires:  python3-docutils pkgconfig(zlib) python3

%description
The lensfun library provides an open source database of photographic lenses and
their characteristics.

%package        devel
Summary:        Development toolkit for lensfun
Requires:       %{name} = %{version}-%{release}
%description    devel
This package contains the libraries and header files needed to build an application
using lensfun.

%package        tools
Summary:        Tools for managing lensfun data
Requires:       python3-lensfun = %{version}-%{release}
%description    tools
This package contains tools for getting lens database updates and managing lenses
adapter in lensfun.

%package -n     python3-lensfun
Summary:        Python3 lensfun bindings
Requires:       %{name} = %{version}-%{release}
%description -n python3-lensfun
Library to correct defects introduced by photographic lenses.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

sed -i.shbang \
  -e "s|^#!/usr/bin/env python3$|#!%{__python3}|g" \
  apps/lensfun-add-adapter \
  apps/lensfun-update-data

%build
%{cmake} \
  -DBUILD_DOC:BOOL=ON \
  -DBUILD_TESTS:BOOL=ON \
  -DCMAKE_BUILD_TYPE:STRING=Release \
  -DCMAKE_INSTALL_DOCDIR:PATH=%{_pkgdocdir}

%cmake_build
%cmake_build --target doc

%install
%cmake_install

install -d %{buildroot}/var/lib/lensfun-updates

rm -fv %{buildroot}%{_bindir}/g-lensfun-update-data \
       %{buildroot}%{_mandir}/man1/g-lensfun-update-data.*

%check
%ctest

%files
%doc README.md
%license docs/cc-by-sa-3.0.txt docs/lgpl-3.0.txt
%{_datadir}/lensfun/
%{_libdir}/{liblensfun.so.%{version},liblensfun.so.1*}
%dir /var/lib/lensfun-updates/

%files devel
%{_pkgdocdir}/{*.html,*.png,*.css,*.js,*.svg}
%{_includedir}/lensfun/
%{_libdir}/liblensfun.so
%{_libdir}/pkgconfig/lensfun.pc

%files -n python3-lensfun
%{python3_sitelib}/lensfun*

%files tools
%{_bindir}/{lensfun-add-adapter,lensfun-update-data}

%files help
%{_mandir}/man1/lensfun-add-adapter.1*
%{_mandir}/man1/lensfun-update-data.1*

%changelog
* Tue Mar 04 2025 Funda Wang <fundawang@yeah.net> - 0.3.4-3
- build with cmake 4.0

* Tue Nov 05 2024 Funda Wang <fundawang@yeah.net> - 0.3.4-2
- adopt to new cmake macro

* Thu Sep 21 2023 xu_ping <707078654@qq.com> - 0.3.4-1
- Upgrade version to 0.3.4

* Fri Feb 17 2023 yaoxin <yaoxin30@h-partners.com> - 0.3.2-23
- Fix build failed

* Thu Jul 21 2022 liyanan <liyanan32@h-partners.com> - 0.3.2-22
- Remove unneeded dependencies python3-devel

* Tue Jun 7 2022 Chenyx <chenyixiong3@huawei.com> - 0.3.2-21
- License compliance rectification

* Fri Jan 14 2022 xu_ping <xuping33@huawei.com> - 0.3.2-20
- Packaging svg files

* Tue Feb 25 2020 fengbing <fengbing7@huawei.com> - 0.3.2-19
- Package init
